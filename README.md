# Tasks of the Test and Interface Team

The **Test and Interface Team** 
* breaks the device down to **modules** which require different technical skills to develop (like "hardware", "software", ...), 
* defines **techincal interfaces** between these modules, and 
* specifies **test procedures** for the modules.

## Membership
To join the Test and Interface Team, please sign in to GitLab, open https://gitlab.com/emergency-rave/test-and-interface-team/team-tasks/ and then click on `Request Access`.

![image](https://gitlab.com/emergency-rave/guideline-team/team-tasks/-/wikis/uploads/7f29b8c4daabce5b058620959422851d/image.png)

## Resources
* [Access to relevant DIN and ISO norms](https://emergency-rave.gitlab.io/about-erave/articles/din-norms-limited-free-access/)
* [Device API in OpenAPI format](https://gitlab.com/emergency-rave/controllers-and-sensors/device-api/-/blob/master/spec/deviceApi.json)
* [Guideline Team Resources](https://gitlab.com/emergency-rave/guideline-team/team-tasks/-/wikis/Resources)
* [Terms and Measurements](https://gitlab.com/emergency-rave/guideline-team/team-tasks/-/wikis/Terms-and-Measurements-(Glossary))
